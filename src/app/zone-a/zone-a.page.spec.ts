import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneAPage } from './zone-a.page';

describe('ZoneAPage', () => {
  let component: ZoneAPage;
  let fixture: ComponentFixture<ZoneAPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneAPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneAPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
